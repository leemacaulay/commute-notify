from searchtweets import ResultStream, gen_rule_payload, load_credentials, collect_results
from requests_oauthlib import OAuth1Session
import yaml
import json
import datetime as dt
import pandas as pd

creds = load_credentials(filename="./credentials.yaml",
                        yaml_key="search_tweets_api",
                        env_overwrite=False)

with open('./credentials.yaml') as file:
    data = yaml.safe_load(file)

consumer_key = data["search_tweets_api"]["consumer_key"]
consumer_secret = data["search_tweets_api"]["consumer_secret"]
access_token = data["search_tweets_api"]["access_token"]
access_token_secret = data["search_tweets_api"]["access_token_secret"]

oauth = OAuth1Session(
    consumer_key,
    client_secret=consumer_secret,
    resource_owner_key=access_token,
    resource_owner_secret=access_token_secret,
)

utc = dt.datetime.utcnow() + dt.timedelta(minutes=-1)
utc_time = utc.strftime("%Y%m%d%H%M")
print("toDate:", utc_time)

two_hours = dt.datetime.utcnow() + dt.timedelta(hours=-2, minutes=-1)
two_hours_prior = two_hours.strftime("%Y%m%d%H%M")
print("fromDate:", two_hours_prior)

rule = gen_rule_payload("(from:northernline OR from:victorialine OR from:tflbusalerts) -has:mentions",from_date=str(two_hours_prior), to_date=str(utc_time), results_per_call=100)
print("rule:", rule)

tweets = collect_results(rule,
                        max_results=100,
                        result_stream_args=creds)

[print(tweet.created_at_datetime, tweet.all_text, end='\n\n') for tweet in tweets[0:10]]

bus_trigger = {'tflbusalerts'}
bus_routes_trigger = {'73', '288', 'closure', 'delays', 'disruption', 'cancelled', 'sorry'}
vic_trigger = {'victorialine'}
northern_trigger = {'northernline'}
tube_routes_trigger = {'closure', 'delays', 'disruption', 'cancelled', 'sorry', 'angel', 'euston', 'oxford', 'warren', 'highbury'}

messages = []

for t in tweets:
    tweet_text = set(t.all_text.lower().split())
    tweet_account = set(t.screen_name.lower().split())
    if len(tweet_text.intersection(bus_routes_trigger)) != 0 and len(tweet_account.intersection(bus_trigger)) != 0:
        messages.append("@lmcly 👋 check https://twitter.com/tflbusalerts for possible delays, [{}]".format(utc_time))
    elif len(tweet_text.intersection(tube_routes_trigger)) != 0 and len(tweet_account.intersection(northern_trigger)) != 0:
        messages.append("@lmcly 👋 check https://twitter.com/northernline for possible delays, [{}]".format(utc_time))
    elif len(tweet_text.intersection(tube_routes_trigger)) != 0 and len(tweet_account.intersection(vic_trigger)) != 0:
        messages.append("@lmcly 👋 check https://twitter.com/victorialine for possible delays, [{}]".format(utc_time))
    else:
        print('This tweet does not match a filter')

for m in messages:
    print("Message:", m)
    params = {"status": m}
    oauth.post("https://api.twitter.com/1.1/statuses/update.json", params=params)